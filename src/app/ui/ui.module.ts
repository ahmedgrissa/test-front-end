import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout/layout.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MyDatePickerModule } from 'mydatepicker';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  imports: [
    CommonModule,
    MyDatePickerModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    BrowserModule,
    NgxSpinnerModule
  ],
  declarations: [
    LayoutComponent,
    HeaderComponent,
    FooterComponent,
  ],
  exports: [
    LayoutComponent
  ]
})
export class UiModule { }
